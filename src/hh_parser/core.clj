(ns hh-parser.core
  (:require [clojure.string :as str :refer [join]]
            [clojure.data.json :as json]
            [clj-http.client :as client]))


(def url "https://api.hh.ru/")


(defn get-json [route params]
  (-> url
      (str route)
      (client/get {:accept :json :query-params params})
      :body
      (json/read-str :key-fn keyword)))

(defn get-region-code [region]
  (->> (get-json "areas/113" {})
       :areas
       (filter #(= (:name %) region))
       first
       :id))

(def get-vacancy (partial get-json "vacancies"))

(defn parse-vacancies
  ([area query]
   (let [params {"area" area "text" query "per_page" "100"}
         res (get-vacancy params)]
     (parse-vacancies params (:pages res) (:page res) res)))

  ([params total-pages page acc]
   (let [new-params (merge params {"page" (inc page)})]
     (if (>= page total-pages)
       acc
       (recur new-params total-pages (inc page)
              (update acc :items concat (:items (get-vacancy new-params))))))))


(defn get-salaries [items]
  (->> (map :salary items)
       (filter #(= (:currency %) "RUR"))
       (map #(if (some? (:from %))
               (:from %)
               (:to %)))))


(defn average [items] (if (zero? (count items))
                        0
                        (double (/ (apply + items) (count items)))))

(defn get-total-query [q]
  (->> (get-vacancy {"text" q "area" "1"})
       :found))

(defn get-lang-info [region-code total lang]
  (let [data (parse-vacancies region-code lang)
        q (count (:items data))]
    (str "\n \n"
         "Язык: " lang "\n"
         "Процент: " (quot q (/ total 100)) "% \n"
         "Количество вакансий: " q "\n"
         "Средняя зарплата: " (format "%,d" (int (average (get-salaries (:items data))))) " руб")))

(defn get-info-for-region [region data]
  (let [c-code (get-region-code region)
        total (apply + (map get-total-query data))]
    (if (nil? c-code)
      (println "Не могу найти такой области!!!")
      (->> data
           (map (partial get-lang-info c-code total))
           (join "\n")
           (str "Область: " region)
           (str "Общее количество: " (apply + (map get-total-query data)) "\n")
           println))))

(def langs '("kotlin" "flutter" "swift" "React Native" "Мобильная разработка 1С"))
(def region-name "Москва")
(get-info-for-region region-name langs)
